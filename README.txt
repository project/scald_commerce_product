Scald Commerce Product
----------------------

Use Commerce Product like media on your content.

Features

* Allows you to select the product you want to embed into your drupal
node/field atom reference with drag and drop
* Allows you to load just available products on your shop
* Allows product selection from SKU
** Automaticaly import product title
** Automaticaly match an image of your product

See a list of Scald providers as separate projects for other great providers
http://drupal.org/node/1895554

Dependecy

To use with Drupal Commerce and Scald.

Next steps

* SKU auto-completion
* import product as atom on cron process
